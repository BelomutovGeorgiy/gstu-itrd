﻿using System;

namespace CP_Core
{
    [Serializable]
    public class SLAE
    {
        public double[,] A { get; set; }
        public double[] B { get; set; }

        public double[,] L { get; private set; }
        public double[] Diagonal { get; private set; }

        public double[] Y { get; set; }
        public double[] X { get; set; }

        private int n = 0;

        public SLAE(double[,] a, double[] b, int n)
        {
            A = a;
            B = b;
            this.n = n;
            Y = new double[n];
            X = new double[n];
            L = new double[n, n];
        }
        public SLAE() { }

        public void DecomposeA()
        {
            Diagonal = new double[n];
            L = new double[n, n];
            int i, j, k;
            double sum = 0;
            for (i = 0; i < n; i++)//столбцы
                for (j = i; j < n; j++)//строки
                {
                    sum = A[j,i];//значение вычисляемого элемента
                    for (k = 0; k < i; k++)//вычитание элементов строки из вычисляемого элемента
                        sum = sum - L[i,k] * Diagonal[k] * L[j,k];
                    if (i == j)//диагональный элемент
                    {
                        if (sum <= 0) new ArgumentException("A is not positive deﬁnite");
                        Diagonal[i] = sum;//диагональный элемент
                        L[i,i] = 1;//диагональ
                    }
                    else L[j,i] = sum / Diagonal[i];//внедиагональный элемент
                }
        }
    }
}
