﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using ProtoBuf;

namespace Labs_DAL.Models
{
    [ProtoContract, Serializable]
    public class Train
    {
        [BsonElement(elementName:"Id")]
        [ProtoMember(1)]
        public int Id { get; set; }
        [ProtoMember(2)]
        public int Number { get; set; }
        [ProtoMember(3)]
        public string Destination { get; set; }
        [ProtoMember(4)]
        public string Origin { get; set; }
        [ProtoMember(5)]
        public DateTime StartTime { get; set; }
        [ProtoMember(6)]
        public DateTime ArriveTime { get; set; }
        [ProtoMember(7)]
        public TrainType TrainType { get; set; }

        public Train() { }

        public override bool Equals(object obj)
        {
            if (!(obj is Train))
            {
                return base.Equals(obj);
            }
            var train = (Train) obj;
            return (train.TrainType == TrainType &&
                    train.ArriveTime == ArriveTime &&
                    train.Destination == Destination &&
                    train.Id == Id &&
                    train.Number == Number &&
                    train.Origin == Origin &&
                    train.StartTime == StartTime);
        }

        protected bool Equals(Train other)
        {
            return (Id == other.Id && Number == other.Number && 
                string.Equals(Destination, other.Destination) && 
                string.Equals(Origin, other.Origin) && 
                StartTime.Equals(other.StartTime) && 
                ArriveTime.Equals(other.ArriveTime) && 
                TrainType == other.TrainType);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id;
                hashCode = (hashCode * 397) ^ Number;
                hashCode = (hashCode * 397) ^ (Destination != null ? Destination.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Origin != null ? Origin.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ StartTime.GetHashCode();
                hashCode = (hashCode * 397) ^ ArriveTime.GetHashCode();
                hashCode = (hashCode * 397) ^ (int) TrainType;
                return hashCode;
            }
        }
    }
}
