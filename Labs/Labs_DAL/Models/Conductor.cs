﻿namespace Labs_DAL.Models
{
    public class Conductor
    {
        public int Id { get; set; }
        public int TrainId { get; set; }
        public string Name { get; set; }
        public string Carriage { get; set; }
        public CarriageType CarType { get; set; }
    }
}
