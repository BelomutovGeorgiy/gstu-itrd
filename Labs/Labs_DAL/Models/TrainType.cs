﻿namespace Labs_DAL.Models
{
    public enum TrainType
    {
        Passenger,
        Fast,
        Express,
        Commodity
    }
}
