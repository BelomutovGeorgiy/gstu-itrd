﻿namespace Labs_DAL.Models
{
    public enum CarriageType
    {
        ReservedSeat,
        Compartment,
        Restaurant,
        PrisonCar,
        Cistern
    }
}
