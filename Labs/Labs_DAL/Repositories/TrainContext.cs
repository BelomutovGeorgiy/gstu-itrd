﻿using Labs_DAL.Models;
using Labs_DAL.Repositories.Abstractions;
using Labs_Shared;
using MongoDB.Driver;

namespace Labs_DAL.Repositories
{
    public class TrainContext : ITrainContext
    {
        private readonly IMongoDatabase _db;

        public TrainContext()
        {
            var client = new MongoClient(Constants.MongoDbConnectionString);
            if (client != null)
            {
                _db = client.GetDatabase(Constants.MongoDbName);
            }
        }

        public IMongoCollection<Train> Trains => _db.GetCollection<Train>("trains");
    }
}
