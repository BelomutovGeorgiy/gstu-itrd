﻿using System;
using System.Collections.Generic;
using Labs_DAL.Models;

namespace Labs_DAL.Repositories
{
    public class TrainRepository : BaseTrainRepository
    {
        public TrainRepository() : base()
        {
            _trains = new List<Train>
            {
                ////new Train
                ////{
                ////    Id = 0,
                ////    Number = 255854,
                ////    ArriveTime = new DateTime(2012,12,20),
                ////    Destination = "silent hill",
                ////    TrainType = TrainType.Commodity,
                ////    StartTime = new DateTime(2000, 12, 11),
                ////    Origin = "home",
                ////},
                ////new Train
                ////{
                ////    Id = 1,
                ////    Number = 255681,
                ////    ArriveTime = new DateTime(2012,12,20),
                ////    Destination = "tuda",
                ////    TrainType = TrainType.Fast,
                ////    StartTime = new DateTime(2000, 12, 11),
                ////    Origin = "suda",
                ////},
                ////new Train
                ////{
                ////    Id = 2,
                ////    Number = 259690,
                ////    ArriveTime = new DateTime(2012,12,20),
                ////    Destination = "moskow",
                ////    TrainType = TrainType.Passenger,
                ////    StartTime = new DateTime(2000, 12, 11),
                ////    Origin = "kair",
                ////},
                ////new Train
                ////{
                ////    Id = 3,
                ////    Number = 152520,
                ////    ArriveTime = new DateTime(2012,12,20),
                ////    Destination = "hell",
                ////    TrainType = TrainType.Passenger,
                ////    StartTime = new DateTime(2000, 12, 11),
                ////    Origin = "heaven",
                ////},
                ////new Train
                ////{
                ////    Id = 4,
                ////    Number = 668452,
                ////    ArriveTime = new DateTime(2012,12,20),
                ////    Destination = "earth",
                ////    TrainType = TrainType.Express,
                ////    StartTime = new DateTime(2000, 12, 11),
                ////    Origin = "mars",
                ////}
            };
        }
    }
}
