﻿using System.Data.Entity;
using Labs_DAL.Models;
using Labs_Shared;

namespace Labs_DAL.Repositories
{
    public class ConductorContext : DbContext
    {
        private DbSet<Conductor> _conductors;

        public ConductorContext() : base(Constants.SqlConnectionString)
        {
        }

        public DbSet<Conductor> Conductors=> Set<Conductor>();
    }
}
