﻿using System;
using System.Collections.Generic;
using System.Linq;
using Labs_DAL.Models;
using Labs_DAL.Repositories.Abstractions;
using MongoDB.Driver;

namespace Labs_DAL.Repositories
{
    public class MongoDbTrainRepository : ITrainRepository
    {
        private readonly ITrainContext _context;

        public MongoDbTrainRepository(ITrainContext context)
        {
            _context = context;
        }

        public IEnumerable<Train> GetAll() => _context.Trains.Find(x => true).ToList();

        public Train GetById(int id) => _context.Trains.Find(Builders<Train>.Filter.Eq("Id", id)).FirstOrDefault();

        public void Update(int id, Train source)
        {
            var filter = Builders<Train>.Filter.Eq(s => s.Id, id);
            var update = Builders<Train>.Update
                .Set(s => s.TrainType, source.TrainType)
                .Set(s => s.ArriveTime, source.ArriveTime)
                .Set(s => s.Destination, source.Destination)
                .Set(s => s.Number, source.Number)
                .Set(s => s.Origin, source.Origin)
                .Set(s => s.StartTime, source.StartTime);

            _context.Trains.UpdateOne(filter, update);
        }

        public void Create(Train source)
        {
            source.Id = _context.Trains.Find(_ => true).ToList().Max(t => t.Id) + 1;
            _context.Trains.InsertOne(source);
        }

        public void Delete(int id)
        {
            _context.Trains.DeleteOne(Builders<Train>.Filter.Eq("Id", id));
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void UpdateList(List<Train> newList)
        {
            throw new NotImplementedException();
        }
    }
}
