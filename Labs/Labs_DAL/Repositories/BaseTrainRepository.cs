﻿using System.Collections.Generic;
using System.Linq;
using Labs_DAL.Models;
using Labs_DAL.Repositories.Abstractions;

namespace Labs_DAL.Repositories
{
    public abstract class BaseTrainRepository : ITrainRepository
    {
        protected List<Train> _trains;

        public BaseTrainRepository() { }

        public void UpdateList(List<Train> newList)
        {
            _trains = newList;
        }

        public IEnumerable<Train> GetAll() => _trains;

        public Train GetById(int id) => _trains.FirstOrDefault(t => t.Id == id);

        public void Update(int id, Train source)
        {
            //// trains.
        }

        public void Create(Train source)
        {
            source.Id = _trains.Count > 0 ? (_trains.Max(t => t.Id) + 1) : 0;
            _trains.Add(source);
        }

        public void Delete(int id)
        {
            _trains.RemoveAll(t => t.Id == id);
        }

        public void Save()
        {
            throw new System.NotImplementedException();
        }
    }
}
