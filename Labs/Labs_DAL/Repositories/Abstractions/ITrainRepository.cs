﻿using Labs_DAL.Models;
using System.Collections.Generic;

namespace Labs_DAL.Repositories.Abstractions
{
    public interface ITrainRepository : IReposirory<Train>
    {
        void UpdateList(List<Train> newList);
    }
}
