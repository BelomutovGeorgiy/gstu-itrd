﻿using Labs_DAL.Models;
using MongoDB.Driver;

namespace Labs_DAL.Repositories.Abstractions
{
    public interface ITrainContext
    {
        IMongoCollection<Train> Trains { get; }
    }
}
