﻿using System.Collections.Generic;
using System.Linq;

namespace Labs_DAL.Repositories.Abstractions
{
    public interface IReposirory<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Update(int id, T source);
        void Create(T source);
        void Delete(int id);
        void Save();
    }
}
