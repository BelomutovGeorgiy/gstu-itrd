﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using Labs_DAL.Models;
using Labs_DAL.Repositories.Abstractions;

namespace Labs_DAL.Repositories
{
    public class ConductorRepository : IConductorRepository
    {
        private readonly ConductorContext _context;

        public ConductorRepository(ConductorContext context)
        {
            _context = context;

            if(GetAll().Count() == 0)
            {
                Create(new Conductor
                {
                    Carriage = "aaua",
                    CarType = CarriageType.Cistern,
                    Id = 0,
                    Name = "Diogen",
                    TrainId = 0
                });
                Create(new Conductor
                {
                    Carriage = "aaua",
                    CarType = CarriageType.PrisonCar,
                    Id = 1,
                    Name = "John",
                    TrainId = 0
                });
                Create(new Conductor
                {
                    Carriage = "aaua",
                    CarType = CarriageType.Compartment,
                    Id = 2,
                    Name = "Matvey",
                    TrainId = 1
                });
                Create(new Conductor
                {
                    Carriage = "aaua",
                    CarType = CarriageType.Restaurant,
                    Id = 3,
                    Name = "Anna",
                    TrainId = 2
                });
                Create(new Conductor
                {
                    Carriage = "aaua",
                    CarType = CarriageType.ReservedSeat,
                    Id = 4,
                    Name = "Anastasiya",
                    TrainId = 2
                });
                Create(new Conductor
                {
                    Carriage = "aaua",
                    CarType = CarriageType.Compartment,
                    Id = 5,
                    Name = "Mariya",
                    TrainId = 3
                });
                Create(new Conductor
                {
                    Carriage = "aaua",
                    CarType = CarriageType.PrisonCar,
                    Id = 6,
                    Name = "Ivan",
                    TrainId = 3
                });
                Create(new Conductor
                {
                    Carriage = "aaua",
                    CarType = CarriageType.ReservedSeat,
                    Id = 7,
                    Name = "Petr",
                    TrainId = 3
                });
            }
        }

        public IEnumerable<Conductor> GetAll() => _context.Conductors;

        public Conductor GetById(int id) => _context.Conductors.FirstOrDefault(c => c.Id == id);

        public void Update(int id, Conductor source)
        {
            source.Id = id;
            _context.Conductors.AddOrUpdate(source);
            _context.SaveChanges();
        }

        public void Create(Conductor source)
        {
            _context.Conductors.Add(source);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var cr = _context.Conductors.FirstOrDefault(e => e.Id == id);
            try
            {
                _context.Conductors.Remove(cr);
                _context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                throw e;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
