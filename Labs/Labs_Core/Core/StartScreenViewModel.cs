﻿using Labs_Core.Trains;
using Labs_Core.Trains.Configuration;

namespace Labs_Core.Core
{
    class StartScreenViewModel : BaseViewModel
    {
        public void LocalMode()
        {
            ShowDialog<TrainsViewModel>(TrainsViewDataMode.LocalFiles);
        }
        public void DoubleBackend()
        {
            ShowDialog<TrainsViewModel>(TrainsViewDataMode.DoubleBackends);
        }
    }
}
