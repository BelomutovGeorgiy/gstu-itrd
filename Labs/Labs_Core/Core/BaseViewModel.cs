﻿using System;
using Caliburn.Micro;
using Labs_Core.Shared.ConfirmDialog;
using Ninject;

namespace Labs_Core.Core
{
    public class BaseViewModel : Screen
    {
        private IWindowManager _windowManager;

        [Inject]
        public void Inject(IWindowManager windowManager)
        {
            _windowManager = windowManager;
        }

        public BaseViewModel()
        {
        }

        protected void ShowDialog<T>(params object[] param) where T : BaseViewModel
        {
            var viewModel = Activator.CreateInstance(typeof(T), param) as T;
            IoC.BuildUp(viewModel);
            _windowManager.ShowDialog(viewModel);
        }

        protected void ShowNewWindow<T>(params object[] param) where T : BaseViewModel
        {
            var viewModel = Activator.CreateInstance(typeof(T), param) as T;
            IoC.BuildUp(viewModel);
            _windowManager.ShowWindow(viewModel);
        }

        protected bool ShowConfirmationDialog(string header, string text)
        {
            var viewModel = new ConfirmDialogViewModel(text);
            _windowManager.ShowDialog(viewModel);
            return viewModel.IsConfirm;
        }

        protected void CloseWithParentUpdate()
        {
               
        }
    }
}
