﻿using System.Net.Http;
using Caliburn.Micro;
using Labs_BLL.Abstractions;
using Labs_BLL.Abstractions.Kafka;
using Labs_BLL.Abstractions.Serialization;
using Labs_BLL.IO;
using Labs_BLL.Kafka;
using Labs_BLL.Serialization;
using Labs_BLL.Services;
using Labs_DAL.Models;
using Labs_DAL.Repositories;
using Labs_DAL.Repositories.Abstractions;
using Ninject.Modules;

namespace Labs_Core.Core
{
    public class CoreModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IWindowManager>().To<WindowManager>();
            Bind<ITrainRepository>().To<TrainRepository>().InSingletonScope();

            Bind<IJsonSerializer<Train>>().To<JsonSerializer<Train>>();
            Bind<IXmlSerializer<Train>>().To<XmlSerializer<Train>>();
            Bind<IProtoBufSerializer<Train>>().To<ProtobufSerializer<Train>>();

            Bind<IFileManager<Train>>().To<TrainFileManager>();

            Bind<IKafkaWrapper<Train>>().To<KafkaWrapper<Train>>();

            Bind<HttpClient>().ToSelf();
            Bind<ITrainClient>().To<TrainClient>();
            Bind<IConductorClient>().To<ConductorClient>();
        }
    }
}
