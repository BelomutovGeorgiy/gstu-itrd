﻿namespace Labs_Core.Shared.Configuration
{
    public enum EditionViewModes
    {
        Create, Edit
    }
}
