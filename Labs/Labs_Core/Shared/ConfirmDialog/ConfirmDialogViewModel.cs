﻿using Labs_Core.Core;

namespace Labs_Core.Shared.ConfirmDialog
{
    class ConfirmDialogViewModel : BaseViewModel
    {
        private string _message;
        private bool _isConfirm;

        public string Message => _message;
        public bool IsConfirm => _isConfirm;

        public ConfirmDialogViewModel(string message)
        {
            _message = message;
        }

        public void Confirm()
        {
            _isConfirm = true;
            TryClose();
        }

        public void Cancel()
        {
            _isConfirm = false;
            TryClose();
        }
    }
}
