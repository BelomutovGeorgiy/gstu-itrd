﻿using System.Collections.Generic;
using System.Linq;
using Labs_Core.Core;
using Labs_Core.Trains.ReadModels;
using Labs_DAL.Models;
using Labs_DAL.Repositories.Abstractions;
using Microsoft.Win32;
using Ninject;
using Labs_BLL.Abstractions;
using Labs_BLL.Abstractions.Kafka;
using Labs_Core.Conductors;
using Labs_Core.Shared.Configuration;
using Labs_Shared;
using Labs_Core.Trains.Configuration;

namespace Labs_Core.Trains
{
    class TrainsViewModel : BaseViewModel
    {
        private ITrainRepository _trainRepository;
        private ITrainClient _client;
        private IFileManager<Train> _fileManager;

        private IKafkaWrapper<Train> _broker;

        private IEnumerable<Conductor> _conductors;
        private Conductor _selectedConductor;
        private IEnumerable<TrainReadModel> _trains;
        private TrainReadModel _selectedTrain;

        private TrainsViewDataMode _mode;

        public TrainsViewModel(TrainsViewDataMode mode)
        {
            _mode = mode;
        }

        [Inject]
        public void Inject(ITrainRepository trainRepository,
            IFileManager<Train> fileManager,
            IKafkaWrapper<Train> broker,
            ITrainClient client)
        {
            _client = client;
            _trainRepository = trainRepository;
            _fileManager = fileManager;
            _broker = broker;
        }

        protected override void OnInitialize()
        {
            SetTrains();
        }

        public bool CanEditConductor => _selectedConductor != null;
        public bool TrainDetailsIsVisible => _selectedTrain != null;
        public bool ConductorsIsVisible => _mode == TrainsViewDataMode.DoubleBackends;
        public bool CanOpenFile => _mode == TrainsViewDataMode.LocalFiles;
        public bool CanSaveFile => _mode == TrainsViewDataMode.LocalFiles;
        public bool CanAddConductor => _mode == TrainsViewDataMode.DoubleBackends;

        public IEnumerable<TrainReadModel> Trains
        {
            get => _trains;
            set
            {
                this._trains = value;
                NotifyOfPropertyChange(() => Trains);
                SelectedTrain = _trains.FirstOrDefault();
            }
        }

        public IEnumerable<Conductor> Conductors
        {
            get => _conductors;
            set
            {
                _conductors = value;
                NotifyOfPropertyChange(() => Conductors);
            }
        }

        public Conductor SelectedConductor
        {
            get => _selectedConductor;
            set
            {
                if (_selectedConductor == value) return;
                _selectedConductor = value;
                NotifyOfPropertyChange(() => SelectedConductor);
                NotifyOfPropertyChange(() => CanEditConductor);
            }
        }

        public TrainReadModel SelectedTrain
        {
            get => _selectedTrain;
            set
            {
                if (_selectedTrain == value) return;
                _selectedTrain = value;
                NotifyOfPropertyChange(() => SelectedTrain);
                NotifyOfPropertyChange(() => TrainDetailsIsVisible);
                if (_selectedTrain != null)
                    if (_mode == TrainsViewDataMode.DoubleBackends)
                    {
                        Conductors = _selectedTrain.Conductors;
                    }
            }
        }

        public void CreateTrain()
        {
            ShowDialog<TrainEditViewModel>(_mode);
            SetTrains();
        }

        public void EditTrain()
        {
            if (_mode == TrainsViewDataMode.LocalFiles)
            {
                var train = _trainRepository.GetById(_selectedTrain.Id);
                ShowDialog<TrainEditViewModel>(_mode, train);
                SetTrains();
            }
            else
            {
                var trainDto = _client.GetById(_selectedTrain.Id);
                var train = new Train
                {
                    TrainType = trainDto.TrainType,
                    Id = trainDto.Id,
                    ArriveTime = trainDto.ArriveTime,
                    Destination = trainDto.Destination,
                    Origin = trainDto.Origin,
                    Number = trainDto.Number,
                    StartTime = trainDto.StartTime
                };
                ShowDialog<TrainEditViewModel>(_mode, train);
                SetTrains();
            }
        }

        public void AddConductor()
        {
            ShowDialog<EditConductorViewModel>(_selectedTrain.Id);
            SetTrains();
        }
        public void EditConductor()
        {
            ShowDialog<EditConductorViewModel>(_selectedConductor, _selectedTrain.Id);
            SetTrains();
        }

        public void OpenFile()
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            if (fileDialog.ShowDialog() == true)
            {
                _trainRepository.UpdateList(_fileManager.ReadFromFile(fileDialog.FileName).ToList());
                SetTrains();
            }
        }

        public void SaveFile()
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.AddExtension = false;
            fileDialog.FileName = "trains";
            if (fileDialog.ShowDialog() == true)
            {
                _fileManager.SaveToFile(fileDialog.FileName, _trainRepository.GetAll());
            }
        }

        public void CreateConsumer()
        {
            ShowNewWindow<TrainConsumerViewModel>();
        }

        public void Transmit()
        {
            _broker.SendMessage(Constants.TrainTopicNameResponse, _trainRepository.GetById(_selectedTrain.Id));
        }

        private void SetTrains()
        {
            if (_mode == TrainsViewDataMode.LocalFiles)
            {
                Trains = _trainRepository.GetAll().Select(t => new TrainReadModel
                {
                    DepartTime = t.ArriveTime,
                    DepartDestinationName = $"{t.Origin}-{t.Destination}",
                    ArriveTime = t.ArriveTime,
                    Id = t.Id,
                    Number = t.Number,
                    Type = t.TrainType,
                    DepartPlace = t.Origin,
                    ArrivePlace = t.Destination
                });
            }
            else
            {
                Trains = _client.GetAll().Select(t => new TrainReadModel
                {
                    DepartTime = t.ArriveTime,
                    DepartDestinationName = $"{t.Origin}-{t.Destination}",
                    ArriveTime = t.ArriveTime,
                    Id = t.Id,
                    Number = t.Number,
                    Type = t.TrainType,
                    DepartPlace = t.Origin,
                    ArrivePlace = t.Destination,
                    Conductors = t.Conductors
                });
            }
        }
    }
}
