﻿using System;
using System.Collections.Generic;
using System.Linq;
using Labs_BLL.Abstractions;
using Labs_Core.Core;
using Labs_Core.Shared.Configuration;
using Labs_Core.Trains.Configuration;
using Labs_DAL.Models;
using Labs_DAL.Repositories.Abstractions;
using Ninject;

namespace Labs_Core.Trains
{
    public class TrainEditViewModel : BaseViewModel
    {
        private ITrainRepository _trainRepository;
        private ITrainClient _client;

        private Train _train;
        private readonly EditionViewModes _mode;
        private readonly TrainsViewDataMode _dataMode;

        private int _trainNumber;
        private string _departPlace;
        private string _arrivePlace;
        private DateTime _departTime;
        private DateTime _arriveTime;
        private TrainType _trainType;

        [Inject]
        public void Inject(ITrainRepository trainRepository,
            ITrainClient client)
        {
            _trainRepository = trainRepository;
            _client = client;
        }

        public int TrainNumber
        {
            get { return _trainNumber; }
            set
            {
                _trainNumber = value;
                NotifyOfPropertyChange(() => TrainNumber);
            }
        }
        public DateTime DepartTime
        {
            get { return _departTime; }
            set
            {
                _departTime = value;
                NotifyOfPropertyChange(() => DepartTime);
            }
        }
        public DateTime ArriveTime
        {
            get { return _arriveTime; }
            set
            {
                _arriveTime = value;
                NotifyOfPropertyChange(() => ArriveTime);
            }
        }
        public string DepartPlace
        {
            get { return _departPlace; }
            set
            {
                _departPlace = value;
                NotifyOfPropertyChange(() => DepartPlace);
            }
        }
        public string ArrivePlace
        {
            get { return _arrivePlace; }
            set
            {
                _arrivePlace = value;
                NotifyOfPropertyChange(() => ArrivePlace);
            }
        }

        public List<TrainType> TrainTypes => Enum.GetValues(typeof(TrainType)).Cast<TrainType>().ToList();

        public TrainType SelectedTrainType
        {
            get { return _trainType; }
            set
            {
                _trainType = value;
                NotifyOfPropertyChange(() => SelectedTrainType);
            }
        }


        public EditionViewModes Mode => _mode;

        public string UpdateButtonText => _mode == EditionViewModes.Create ? TrainsViewResources.CreateButton_Text : TrainsViewResources.UpdateButton_Text;
        public bool TrainNumberIsVisible => _mode == EditionViewModes.Create;
        public bool DeleteButtonIsVisible => _mode == EditionViewModes.Edit;

        public TrainEditViewModel(TrainsViewDataMode dataMode, Train train)
        {
            _dataMode = dataMode;
            _train = train;
            _mode = EditionViewModes.Edit;

            TrainNumber = _train.Number;
            DepartTime = _train.StartTime;
            ArriveTime = _train.ArriveTime;
            DepartPlace = _train.Origin;
            ArrivePlace = _train.Destination;
            SelectedTrainType = _train.TrainType;
        }

        public TrainEditViewModel(TrainsViewDataMode dataMode)
        {
            _dataMode = dataMode;
            _mode = EditionViewModes.Create;
        }

        public void UpdateCommand()
        {
            if (_mode == EditionViewModes.Edit)
            {
                _train.ArriveTime = _arriveTime;
                _train.Destination = _arrivePlace;
                _train.Origin = _departPlace;
                _train.StartTime = _departTime;
                _train.TrainType = _trainType;
                if (_dataMode == TrainsViewDataMode.DoubleBackends)
                {
                    _client.Update(_train.Id, _train);
                }
                else
                {
                    _trainRepository.Update(_train.Id, _train);
                }

            }
            else
            {
                var newTrain = new Train
                {
                    ArriveTime = _arriveTime,
                    Number = _trainNumber,
                    Destination = _arrivePlace,
                    Origin = _departPlace,
                    StartTime = _departTime,
                    TrainType = SelectedTrainType
                };
                if (_dataMode == TrainsViewDataMode.DoubleBackends)
                {
                    _client.Create(newTrain);
                }
                else
                {
                    _trainRepository.Create(newTrain);
                }
            }
            TryClose();
        }

        public void DeleteCommand()
        {
            if (ShowConfirmationDialog("wut", "are u sure, u want to delete it"))
            {
                if (_dataMode == TrainsViewDataMode.DoubleBackends)
                {
                    _client.Delete(_train.Id);
                }
                else
                {
                    _trainRepository.Delete(_train.Id);
                }
                TryClose();
            }
        }
    }
}
