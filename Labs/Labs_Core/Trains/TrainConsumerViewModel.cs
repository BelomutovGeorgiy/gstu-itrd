﻿using Labs_BLL.Abstractions.Kafka;
using Labs_Core.Core;
using Labs_Core.Trains.ReadModels;
using Labs_DAL.Models;
using Labs_Shared;
using Ninject;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Labs_Core.Trains
{
    public class TrainConsumerViewModel : BaseViewModel
    {
        private IKafkaWrapper<Train> _broker;

        private IEnumerable<TrainReadModel> _trains;

        private List<Train> _trainsStorage;

        public IEnumerable<TrainReadModel> RecentTrains
        {
            get { return _trains; }
            set
            {
                this._trains = value;
                NotifyOfPropertyChange(() => RecentTrains);
            }
        }

        [Inject]
        public void Inject(IKafkaWrapper<Train> broker)
        {
            _broker = broker;
        }

        protected override void OnInitialize()
        {
            _trainsStorage = new List<Train>();
            Task.Run(() => _broker.Subscribe(Constants.TrainTopicNameResponse, msg => GetCommodityTrainHandler(msg), CancellationToken.None));
        }

        public void GetCommodityTrainHandler(Train train)
        {
            _trainsStorage.Add(train);
            RecentTrains = _trainsStorage.Select(t => new TrainReadModel
            {
                DepartTime = t.ArriveTime,
                DepartDestinationName = $"{t.Origin}-{t.Destination}",
                ArriveTime = t.ArriveTime,
                Id = t.Id,
                Number = t.Number,
                Type = t.TrainType,
                DepartPlace = t.Origin,
                ArrivePlace = t.Destination
            });
        }

        public void Request()
        {
            Refresh();
        }
    }
}
