﻿using System;
using System.Collections.Generic;
using Labs_DAL.Models;

namespace Labs_Core.Trains.ReadModels
{
    public class TrainReadModel
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string DepartDestinationName { get; set; }
        public TrainType Type { get; set; }
        public DateTime DepartTime { get; set; }
        public DateTime ArriveTime { get; set; }
        public string DepartPlace { get; set; }
        public string ArrivePlace { get; set; }
        public IEnumerable<Conductor> Conductors { get; set; }
    }
}
