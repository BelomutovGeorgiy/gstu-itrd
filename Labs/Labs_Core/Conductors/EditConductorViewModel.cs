﻿using System;
using System.Collections.Generic;
using System.Linq;
using Labs_BLL.Abstractions;
using Labs_Core.Core;
using Labs_Core.Shared.Configuration;
using Labs_DAL.Models;
using Ninject;

namespace Labs_Core.Conductors
{
    class EditConductorViewModel : BaseViewModel
    {
        private EditionViewModes _mode;
        private IConductorClient _client;
        private Conductor _conductor;

        public string UpdateButtonText => _mode == EditionViewModes.Create ? "Create" : "Update";
        public bool DeleteButtonIsVisible => _mode == EditionViewModes.Edit;

        public string Name
        {
            get => _conductor.Name;
            set
            {
                _conductor.Name = value;
                NotifyOfPropertyChange(() =>  Name);
            }
        }
        public string Carriage
        {
            get => _conductor.Carriage;
            set
            {
                _conductor.Carriage = value;
                NotifyOfPropertyChange(() => Carriage);
            }
        }

        public List<CarriageType> CarriageTypes => Enum.GetValues(typeof(CarriageType)).Cast<CarriageType>().ToList();

        public CarriageType SelectedCarriageType
        {
            get { return _conductor.CarType; }
            set
            {
                _conductor.CarType = value;
                NotifyOfPropertyChange(() => SelectedCarriageType);
            }
        }

        [Inject]
        public void Inject(IConductorClient client)
        {
            _client = client;
        }

        public EditConductorViewModel(Conductor conductor, int trainId)
        {
            _mode = EditionViewModes.Edit;
            _conductor = conductor;

        }
        public EditConductorViewModel(int trainId)
        {
            _mode = EditionViewModes.Create;
            _conductor = new Conductor
            {
                TrainId = trainId
            };
        }

        public void Update()
        {
            if (_mode == EditionViewModes.Create)
            {
                _client.Create(_conductor);
            }
            else
            {
                _client.Update(_conductor.Id, _conductor);
            }
            TryClose();
        }

        public void Delete()
        {
            if (ShowConfirmationDialog("wut", "are u sure, u want to delete it"))
            {
                _client.Delete(_conductor.Id);
                TryClose();
            }
        }
    }
}
