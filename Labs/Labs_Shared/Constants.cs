﻿namespace Labs_Shared
{
    public static class Constants
    {
        public const string TrainTopicNameCommand = "b_name_command";
        public const string TrainMessageRequest = "get_boy_name";

        public const string TrainTopicNameResponse = "b_name_response";

        public const string MongoDbConnectionString = "mongodb://127.0.0.1:27017";
        public const string MongoDbName = "trainsDB";

        public const string SqlConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=ConductorDB_Belomutov_George;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
    }
}
