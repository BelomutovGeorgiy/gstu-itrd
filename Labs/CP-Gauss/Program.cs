﻿using CP_Core;
using System;
using System.Diagnostics;
using System.IO;
namespace GaussMethod
{
    class Program
    {
        static string Mwritepath = @"..\..\..\MatrixData.txt";
        static string Awritepath = @"..\..\..\ArrayData.txt";
        static string ASavingpath = @"..\..\..\Linear.txt";
        static double[] x;
        static double[] b;
        static double[,] a;
        static int n = 3000;
        static void SaveToFile()
        {
            Console.WriteLine("Saving...");
            //Stream MStream = new Stream();
            FileStream outStream1 = new FileStream(ASavingpath, FileMode.OpenOrCreate);
            using (StreamWriter sw = new StreamWriter(outStream1))
            {
                for (int i = 0; i < a.GetLength(0); i++)
                {
                    sw.WriteLine(x[i]);
                }
                sw.Close();
            }
            outStream1.Close();
        }
        static void ReadMatrix()
        {
            String fileName = Mwritepath;
            StreamReader data;
            a = new double[n, n];
            try
            {
                data = new StreamReader(fileName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return;//аварийное завершение программы
            }
            string currentLine;
            string[] partsOfLine;
            for (int i = 0; i < n; i++)
            {
                try
                {
                    currentLine = data.ReadLine();
                    partsOfLine = currentLine.Split(' ');
                    for (int j = 0; j < n; j++)
                    {
                        a[i, j] = Convert.ToDouble(partsOfLine[j]);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Неверно записаны данные в файле.\n" + e.ToString());
                    return;//аварийное завершение программы
                }
            }
        }
        static void ReadAnsvers()
        {
            String fileName = Awritepath;
            StreamReader data;
            b = new double[n];
            x = new double[n];
            try
            {
                data = new StreamReader(fileName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return;//аварийное завершение программы
            }
            string currentLine;
            for (int i = 0; i < n; i++)
            {
                try
                {
                    currentLine = data.ReadLine();
                    b[i] = Convert.ToDouble(currentLine);
                    x[i] = 0;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Неверно записаны данные в файле.\n" + e.ToString());
                    return;//аварийное завершение программы
                }
            }
        }
        static void Main(string[] args)
        {
            n = Convert.ToInt32(args[0]);
            ReadMatrix();
            ReadAnsvers();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            x = HolezkohoMethod();
            sw.Stop();
            printArray(x);
            SaveToFile();
            Console.WriteLine("Gauss Done! it's take " + sw.Elapsed.TotalMilliseconds + ". Data Saved to Linear.txt");
        }
        public static void printArray(double[] args)
        {
            foreach (double o in args)
            {
                Console.WriteLine(o + ";");
            }
            Console.WriteLine();
        }
        public static double[] HolezkohoMethod()
        {
            var slae = new SLAE(a, b, n);
            slae.DecomposeA();

            for (int i = 0; i < n; i++)
            {
                if (i == 0)
                    slae.Y[i] = slae.B[i];
                else
                {
                    double sum = 0;
                    for (int j = 0; j < i - 1; j++)
                        sum += slae.L[i, j] * slae.Y[j];
                    slae.Y[i] = slae.B[i] - sum;
                }

                slae.X[i] = slae.Y[i] / slae.Diagonal[i];
            }
            return slae.X;
        }
    }
}