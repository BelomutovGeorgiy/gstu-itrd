﻿using CP_Core;
using MPI;
using System;
using System.Diagnostics;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static string Mwritepath = @"..\..\..\MatrixData.txt";
        static string Awritepath = @"..\..\..\ArrayData.txt";
        static string ASavingpath = @"..\..\..\Parrallel.txt";
        const double eps = 1e-7;
        static bool converge(double[] a, double[] b)
        {
            double difmax = 0;
            for (int i = 0; i < a.Length; i++)
                if (Math.Abs(a[i] - b[i]) > difmax)
                    difmax = Math.Abs(a[i] - b[i]);
            if (difmax < eps)
                return true;
            else
                return false;
        }
        private static double[] x = new double[10000];
        private static double[] p = new double[10000];
        private static double[] b = new double[10000];
        private static double[,] a;

        static int n = 3000;
        static void SaveToFile()
        {
            Console.WriteLine("Saving...");
            //Stream MStream = new Stream();
            FileStream outStream1 = new FileStream(ASavingpath, FileMode.OpenOrCreate);
            using (StreamWriter sw = new StreamWriter(outStream1))
            {
                for (int i = 0; i < a.GetLength(0); i++)
                {
                    sw.WriteLine(x[i]);
                }
                sw.Close();
            }
            outStream1.Close();
        }
        static void ReadMatrix()
        {
            String fileName = Mwritepath;
            StreamReader data;
            a = new double[n, n];
            try
            {
                data = new StreamReader(fileName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return;//аварийное завершение программы
            }
            string currentLine;
            string[] partsOfLine;
            for (int i = 0; i < n; i++)
            {
                try
                {
                    currentLine = data.ReadLine();
                    partsOfLine = currentLine.Split(' ');
                    for (int j = 0; j < n; j++)
                    {
                        a[i, j] = Convert.ToDouble(partsOfLine[j]);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Неверно записаны данные в файле.\n" + e.ToString());
                    return;//аварийное завершение программы
                }
            }
        }
        static void ReadAnsvers()
        {
            String fileName = Awritepath;
            StreamReader data;
            b = new double[n];
            x = new double[n];
            p = new double[n];
            try
            {
                data = new StreamReader(fileName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return;//аварийное завершение программы
            }
            string currentLine;
            for (int i = 0; i < n; i++)
            {
                try
                {
                    currentLine = data.ReadLine();
                    b[i] = Convert.ToDouble(currentLine);
                    x[i] = 0;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Неверно записаны данные в файле.\n" + e.ToString());
                    return;//аварийное завершение программы
                }
            }
        }
        static void Main(string[] args)
        {
            if (args.Length > 0)
                n = Convert.ToInt32(args[0]);
            // using (new MPI.Environment(ref args))
            // {
            // Intracommunicator comm = Communicator.world;
            ReadMatrix();
            ReadAnsvers();
            //Console.ReadKey();
            Stopwatch sw = new Stopwatch();
            using (new MPI.Environment(ref args))
            {
                Intracommunicator comm = Communicator.world;
                sw.Start();
                x = HolezkohoMethod(comm);
                sw.Stop();
                if (comm.Rank == comm.Size - 1)
                {
                    printArray(x);
                    SaveToFile();
                    Console.WriteLine(" LDL*t Done! it's take " + sw.Elapsed.TotalMilliseconds + ". Results saved to Parrallel.txt");
                }
            }
        }
        public static void printArray(double[] args)
        {
            foreach (double o in args)
            {
                Console.WriteLine(o + ";");
            }
            Console.WriteLine();
        }
        public static double[] HolezkohoMethod(Intracommunicator comm)
        {
            if (comm.Rank == 0)
            {
                var slae = new SLAE(a, b, n);
                slae.DecomposeA();
                comm.Send(slae, 1, 0);
                return slae.X;
            }
            else
            {
                var slae = comm.Receive<SLAE>(Communicator.anySource, 0);
                int i = comm.Rank - 1;
                if (i == 0)
                    slae.Y[i] = slae.B[i];
                else
                {
                    double sum = 0;
                    for (int j = 0; j < i - 1; j++)
                        sum += slae.L[i, j] * slae.Y[j];
                    slae.Y[i] = slae.B[i] - sum;
                }
                
                slae.X[i] = slae.Y[i] / slae.Diagonal[i];
                if (comm.Rank != comm.Size - 1)
                {
                    comm.Send(slae, (comm.Rank + 1) % comm.Size, 0);
                }
                return slae.X;
            }
        }
    }
}