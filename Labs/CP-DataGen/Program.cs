﻿using System;
using System.IO;
namespace CP_DataGenerator
{
    class Program
    {
        static string Mwritepath = @"..\..\..\MatrixData.txt";
        static string Awritepath = @"..\..\..\ArrayData.txt";
        static double[,] a;
        static double[] b;
        static int n = 15000;
        static void initArrays()
        {
            Random rnd = new Random();
            b = new double[n];
            for (int i = 0; i < n; i++)
        {
                b[i] = rnd.Next(-10, 10);
            }
            Console.WriteLine("Working...");
        }
        static void initMatrix()
        {
            Random rnd = new Random();
            a = new double[n, n];
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < i; j++)
                {
                    if (i != j)
                    {
                        a[i, j] = rnd.Next(-10, 10);
                        a[j, i] = a[i,j];
                    }
                    else
                        a[i, j] = 0;
                }
            }
            for (int i = 0; i < a.GetLength(0); i++)
            {
                double sum = 0f;
                for (int j = 0; j < a.GetLength(0); j++)
                    sum += Math.Abs(a[i, j]);
                {
                    a[i, i] = sum;
                }
            }
        }
        static void SaveToFile()
        {
            Console.WriteLine("Saving...");
            //Stream MStream = new Stream();
            FileStream outStream1 = new FileStream(Mwritepath, FileMode.OpenOrCreate);
            FileStream outStream2 = new FileStream(Awritepath, FileMode.OpenOrCreate);
            using (StreamWriter sw = new StreamWriter(outStream1))
            {
                for (int i = 0; i < a.GetLength(0); i++)
                {
                    for (int j = 0; j < a.GetLength(1); j++)
                    {
                        sw.Write(a[i, j] + " ");
                    }
                    sw.WriteLine();
                }
                sw.Close();
            }
            outStream1.Close();
            using (StreamWriter sw = new StreamWriter(outStream2))
            {
                for (int i = 0; i < a.GetLength(0); i++)
                {
                    sw.WriteLine(b[i]);
                }
                sw.Close();
            }
            outStream2.Close();
        }
        static void ChangeN()
        {
            Console.Clear();
            Console.WriteLine("Insert new Count:");
            n = Convert.ToInt32(Console.ReadLine());
        }
        static int menu()
        {
            ConsoleKeyInfo k = new ConsoleKeyInfo();
            do
            {
                Console.Clear();
                Console.WriteLine("Matrix Gen 1.0");
                Console.WriteLine($"Variable Count is: {n}");
                Console.WriteLine("You turn");
                Console.WriteLine("1:Generate Matrix");
                Console.WriteLine("2:Generate Ansvrs");
                Console.WriteLine("3:Save To File");
                Console.WriteLine("4:Change Count(n)");
                Console.WriteLine("9:Exit");
                k = Console.ReadKey();
                switch (k.KeyChar)
                {
                    case '1': initMatrix(); break;
                    case '2': initArrays(); break;
                    case '3': SaveToFile(); break;
                    case '4': ChangeN(); break;
                    default: break;
                }
            } while (k.KeyChar != '9');
            return -1;
        }
        static void Main(string[] args)
        {
            n = Convert.ToInt32(args[0]);
            menu();
        }
    }
}