﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using Labs_DAL.Models;
using Labs_DAL.Repositories.Abstractions;
using System.Linq;
using Labs_PrimaryBackend.Models;
using Labs_PrimaryBackend.SoapBackendService;

namespace Labs_PrimaryBackend.Controllers
{
    public class TrainController : ApiController
    {
        private readonly ITrainRepository _trainRepository;
        private ConductorServiceClient _client;

        public TrainController(ITrainRepository trainRepository,
            ConductorServiceClient client)
        {
            _trainRepository = trainRepository;
            _client = client;
        }

        // GET: api/Train
        public HttpResponseMessage Get()
        {
            var trains = _trainRepository.GetAll();
            var trainsDto = trains.Select(t => new TrainDTO
            {
                ArriveTime = t.ArriveTime,
                Conductors = _client.GetByTrainId(t.Id),
                Destination = t.Destination,
                Id = t.Id,
                Number = t.Number,
                Origin = t.Origin,
                StartTime = t.StartTime,
                TrainType = t.TrainType
            });

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ObjectContent<IEnumerable<TrainDTO>>(trainsDto, new JsonMediaTypeFormatter())
            };
            return response;

        }

        // GET: api/Train/5
        public HttpResponseMessage Get(int id)
        {
            var t = _trainRepository.GetById(id);
            if (t != null)
            {
                var trainDto = new TrainDTO
                {
                    ArriveTime = t.ArriveTime,
                    Conductors = _client.GetByTrainId(t.Id),
                    Destination = t.Destination,
                    Id = t.Id,
                    Number = t.Number,
                    Origin = t.Origin,
                    StartTime = t.StartTime,
                    TrainType = t.TrainType
                };

                var response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ObjectContent<TrainDTO>(trainDto, new JsonMediaTypeFormatter())
                };
                return response;
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            } 
        }

        // POST: api/Train
        public HttpResponseMessage Post([FromBody]Train value)
        {
            _trainRepository.Create(value);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        // PUT: api/Train/5
        public HttpResponseMessage Put(int id, [FromBody]Train value)
        {
            _trainRepository.Update(id, value);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        // DELETE: api/Train/5
        public HttpResponseMessage Delete(int id)
        {
            _trainRepository.Delete(id);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}
