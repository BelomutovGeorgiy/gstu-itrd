﻿using Labs_DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Labs_PrimaryBackend.Controllers
{
    public class ConductorController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<Conductor> Get()
        {
            var client = new SoapBackendService.ConductorServiceClient();
            var result = client.GetAll();
            client.Close();
            return result;
        }

        // GET api/<controller>/5
        public Conductor Get(int id)
        {
            var client = new SoapBackendService.ConductorServiceClient();
            var result = client.GetById(id);
            client.Close();
            return result;
        }

        // POST api/<controller>
        public void Post([FromBody]Conductor value)
        {
            var client = new SoapBackendService.ConductorServiceClient();
            client.Add(value);
            client.Close();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]Conductor value)
        {
            var client = new SoapBackendService.ConductorServiceClient();
            client.Update(id,value);
            client.Close();
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            var client = new SoapBackendService.ConductorServiceClient();
            client.Delete(id);
            client.Close();
        }
    }
}