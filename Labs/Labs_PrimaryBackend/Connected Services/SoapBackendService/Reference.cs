﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Labs_PrimaryBackend.SoapBackendService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="SoapBackendService.IConductorService")]
    public interface IConductorService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConductorService/GetById", ReplyAction="http://tempuri.org/IConductorService/GetByIdResponse")]
        Labs_DAL.Models.Conductor GetById(int value);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConductorService/GetById", ReplyAction="http://tempuri.org/IConductorService/GetByIdResponse")]
        System.Threading.Tasks.Task<Labs_DAL.Models.Conductor> GetByIdAsync(int value);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConductorService/GetAll", ReplyAction="http://tempuri.org/IConductorService/GetAllResponse")]
        Labs_DAL.Models.Conductor[] GetAll();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConductorService/GetAll", ReplyAction="http://tempuri.org/IConductorService/GetAllResponse")]
        System.Threading.Tasks.Task<Labs_DAL.Models.Conductor[]> GetAllAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConductorService/GetByTrainId", ReplyAction="http://tempuri.org/IConductorService/GetByTrainIdResponse")]
        Labs_DAL.Models.Conductor[] GetByTrainId(int trainId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConductorService/GetByTrainId", ReplyAction="http://tempuri.org/IConductorService/GetByTrainIdResponse")]
        System.Threading.Tasks.Task<Labs_DAL.Models.Conductor[]> GetByTrainIdAsync(int trainId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConductorService/Delete", ReplyAction="http://tempuri.org/IConductorService/DeleteResponse")]
        void Delete(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConductorService/Delete", ReplyAction="http://tempuri.org/IConductorService/DeleteResponse")]
        System.Threading.Tasks.Task DeleteAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConductorService/Add", ReplyAction="http://tempuri.org/IConductorService/AddResponse")]
        void Add(Labs_DAL.Models.Conductor item);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConductorService/Add", ReplyAction="http://tempuri.org/IConductorService/AddResponse")]
        System.Threading.Tasks.Task AddAsync(Labs_DAL.Models.Conductor item);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConductorService/Update", ReplyAction="http://tempuri.org/IConductorService/UpdateResponse")]
        void Update(int id, Labs_DAL.Models.Conductor item);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConductorService/Update", ReplyAction="http://tempuri.org/IConductorService/UpdateResponse")]
        System.Threading.Tasks.Task UpdateAsync(int id, Labs_DAL.Models.Conductor item);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IConductorServiceChannel : Labs_PrimaryBackend.SoapBackendService.IConductorService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ConductorServiceClient : System.ServiceModel.ClientBase<Labs_PrimaryBackend.SoapBackendService.IConductorService>, Labs_PrimaryBackend.SoapBackendService.IConductorService {
        
        public ConductorServiceClient() {
        }
        
        public ConductorServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ConductorServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ConductorServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ConductorServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public Labs_DAL.Models.Conductor GetById(int value) {
            return base.Channel.GetById(value);
        }
        
        public System.Threading.Tasks.Task<Labs_DAL.Models.Conductor> GetByIdAsync(int value) {
            return base.Channel.GetByIdAsync(value);
        }
        
        public Labs_DAL.Models.Conductor[] GetAll() {
            return base.Channel.GetAll();
        }
        
        public System.Threading.Tasks.Task<Labs_DAL.Models.Conductor[]> GetAllAsync() {
            return base.Channel.GetAllAsync();
        }
        
        public Labs_DAL.Models.Conductor[] GetByTrainId(int trainId) {
            return base.Channel.GetByTrainId(trainId);
        }
        
        public System.Threading.Tasks.Task<Labs_DAL.Models.Conductor[]> GetByTrainIdAsync(int trainId) {
            return base.Channel.GetByTrainIdAsync(trainId);
        }
        
        public void Delete(int id) {
            base.Channel.Delete(id);
        }
        
        public System.Threading.Tasks.Task DeleteAsync(int id) {
            return base.Channel.DeleteAsync(id);
        }
        
        public void Add(Labs_DAL.Models.Conductor item) {
            base.Channel.Add(item);
        }
        
        public System.Threading.Tasks.Task AddAsync(Labs_DAL.Models.Conductor item) {
            return base.Channel.AddAsync(item);
        }
        
        public void Update(int id, Labs_DAL.Models.Conductor item) {
            base.Channel.Update(id, item);
        }
        
        public System.Threading.Tasks.Task UpdateAsync(int id, Labs_DAL.Models.Conductor item) {
            return base.Channel.UpdateAsync(id, item);
        }
    }
}
