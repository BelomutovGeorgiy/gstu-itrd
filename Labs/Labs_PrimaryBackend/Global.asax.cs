﻿using System.Reflection;
using System.Web.Http;
using Labs_DAL.Repositories;
using Labs_DAL.Repositories.Abstractions;
using Ninject;
using Ninject.Web.WebApi;

namespace Labs_PrimaryBackend
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
