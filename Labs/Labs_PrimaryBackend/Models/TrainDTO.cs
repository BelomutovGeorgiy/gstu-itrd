﻿using Labs_DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Labs_PrimaryBackend.Models
{
    public class TrainDTO
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Destination { get; set; }
        public string Origin { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime ArriveTime { get; set; }
        public TrainType TrainType { get; set; }

        public IEnumerable<Conductor> Conductors { get; set; }
    }
}