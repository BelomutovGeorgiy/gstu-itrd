﻿using System.Collections.Generic;
using System.Linq;
using Labs_DAL.Models;
using Labs_DAL.Repositories.Abstractions;

namespace Labs_SecondaryBackup
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ConductorService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ConductorService.svc or ConductorService.svc.cs at the Solution Explorer and start debugging.
    public class ConductorService : IConductorService
    {
        private readonly IConductorRepository _conductorRepository;

        public ConductorService(IConductorRepository conductorRepository)
        {
            _conductorRepository = conductorRepository;
        }

        public Conductor GetById(int value)
        {
            return _conductorRepository.GetById(value);
        }

        public IEnumerable<Conductor> GetAll()
        {
            return _conductorRepository.GetAll();
        }

        public IEnumerable<Conductor> GetByTrainId(int trainId)
        {
            return _conductorRepository.GetAll().Where(c => c.TrainId == trainId);
        }

        public void Delete(int id)
        {
            _conductorRepository.Delete(id);
        }

        public void Add(Conductor item)
        {
            _conductorRepository.Create(item);
        }

        public void Update(int id, Conductor item)
        {
           _conductorRepository.Update(id, item);
        }
    }
}
