﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Labs_DAL.Models;

namespace Labs_SecondaryBackup
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IConductorService" in both code and config file together.
    [ServiceContract]
    public interface IConductorService
    {

        [OperationContract]
        Conductor GetById(int value);

        [OperationContract]
        IEnumerable<Conductor> GetAll();

        [OperationContract]
        IEnumerable<Conductor> GetByTrainId(int trainId);

        [OperationContract]
        void Delete(int id);

        [OperationContract]
        void Add(Conductor item);

        [OperationContract]
        void Update(int id, Conductor item);
    }
}
