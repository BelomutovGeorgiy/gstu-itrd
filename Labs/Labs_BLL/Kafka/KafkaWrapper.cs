﻿using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using Labs_BLL.Abstractions.Kafka;
using Labs_BLL.Abstractions.Serialization;
using Ninject;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Labs_BLL.Kafka
{
    public class KafkaWrapper<T> : IKafkaWrapper<T>, IDisposable where T: class
    {
        private readonly Producer<Null, string> _producer;
        private Consumer<Null, string> _consumer;

        private readonly IDictionary<string, object> _producerConfig;
        private readonly IDictionary<string, object> _consumerConfig;

        protected IJsonSerializer<T> _jsonSerializer;

        [Inject]
        public void Inject(IJsonSerializer<T> jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
        }

        public KafkaWrapper() : this("localhost") { }

        public KafkaWrapper(string host)
        {
            _producerConfig = new Dictionary<string, object> { { "bootstrap.servers", host } };
            _consumerConfig = new Dictionary<string, object>
            {
                { "group.id", "custom-group"},
                { "bootstrap.servers", host }
            };

            _producer = new Producer<Null, string>(_producerConfig, null, new StringSerializer(Encoding.UTF8));
        }

        public void Dispose()
        {
            _producer?.Dispose();
            _consumer?.Dispose();
        }

        public void SendMessage(string topic, T message)
        {
            string msg = _jsonSerializer.Serialize(message);
            _producer.ProduceAsync(topic, null, msg).Wait();
        }

        public void Subscribe(string topic, Action<T> action, CancellationToken token)
        {
            var msgBus = new KafkaWrapper<T>();
            using (msgBus._consumer = new Consumer<Null, string>(_consumerConfig, null, new StringDeserializer(Encoding.UTF8)))
            {
                msgBus._consumer.Assign(new List<TopicPartitionOffset> { new TopicPartitionOffset(topic, 0, -1) });

                while (true)
                {
                    if (token.IsCancellationRequested)
                        break;

                    Message<Null, string> msg;
                    if (msgBus._consumer.Consume(out msg, TimeSpan.FromMilliseconds(10)))
                    {
                        action(_jsonSerializer.Deserialize(msg.Value));
                    }
                }
            }
        }
    }
}
