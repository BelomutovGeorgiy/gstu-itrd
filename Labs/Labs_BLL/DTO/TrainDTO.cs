﻿using System;
using System.Collections.Generic;
using Labs_DAL.Models;

namespace Labs_BLL.DTO
{
    public class TrainDTO
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Destination { get; set; }
        public string Origin { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime ArriveTime { get; set; }
        public TrainType TrainType { get; set; }

        public IEnumerable<Conductor> Conductors { get; set; }
    }
}