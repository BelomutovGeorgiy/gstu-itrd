﻿using System.Net.Http;
using Labs_BLL.Abstractions;
using Labs_DAL.Models;
using Ninject;

namespace Labs_BLL.Services
{
    public class ConductorClient : IConductorClient
    {
        private HttpClient _client;

        [Inject]
        public void Inject(HttpClient client)
        {
            _client = client;
        }

        public void Create(Conductor item)
        {
            _client.PostAsJsonAsync("http://localhost:51403/api/Conductor", item).Wait();
        }

        public void Update(int id, Conductor item)
        {
            _client.PutAsJsonAsync($"http://localhost:51403/api/Conductor/{id}", item).Wait();
        }

        public void Delete(int id)
        {
            _client.DeleteAsync($"http://localhost:51403/api/Conductor/{id}").Wait();
        }
    }
}
