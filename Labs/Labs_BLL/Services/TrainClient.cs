﻿using System.Collections.Generic;
using System.Net.Http;
using Labs_BLL.Abstractions;
using Labs_BLL.DTO;
using Labs_DAL.Models;
using Newtonsoft.Json;
using Ninject;

namespace Labs_BLL.Services
{
    public class TrainClient : ITrainClient
    {
        private HttpClient _client;

        [Inject]
        public void Inject(HttpClient client)
        {
            _client = client;
        }

        public IEnumerable<TrainDTO> GetAll()
        {
            var resp = _client.GetStringAsync("http://localhost:51403/api/Train").Result;
            return JsonConvert.DeserializeObject<IEnumerable<TrainDTO>>(resp);
        }

        public TrainDTO GetById(int id)
        {
            var resp = _client.GetStringAsync($"http://localhost:51403/api/Train/{id}").Result;
            return JsonConvert.DeserializeObject<TrainDTO>(resp);
        }

        public void Create(Train train)
        {
           _client.PostAsJsonAsync("http://localhost:51403/api/Train", train).Wait();
        }

        public void Delete(int id)
        {
           _client.DeleteAsync($"http://localhost:51403/api/Train/{id}").Wait();
        }

        public void Update(int id, Train train)
        {
            _client.PutAsJsonAsync($"http://localhost:51403/api/Train/{id}", train).Wait();
        }
    }
}
