﻿using System;
using System.Collections.Generic;
using System.IO;
using Labs_BLL.Abstractions.Serialization;
using ProtoBuf;

namespace Labs_BLL.Serialization
{
    public class ProtobufSerializer<T> : IProtoBufSerializer<T> where T : class
    {
        public T Deserialize(string source)
        {
            var arr = Convert.FromBase64String(source);
            return Serializer.Deserialize<T>(new MemoryStream(arr));
        }

        public IEnumerable<T> DeserializeAll(string source)
        {
            var arr = Convert.FromBase64String(source);
            return Serializer.Deserialize<IEnumerable<T>>(new MemoryStream(arr));
        }

        public string Serialize(T source)
        {
            MemoryStream ms = new MemoryStream();
            Serializer.Serialize(ms, source);
            return Convert.ToBase64String(ms.GetBuffer(), 0, (int)ms.Length);
        }

        public string SerializeAll(IEnumerable<T> source)
        {
            MemoryStream ms = new MemoryStream();
            Serializer.Serialize(ms, source);
            return Convert.ToBase64String(ms.GetBuffer(), 0, (int) ms.Length);
        }
    }
}
