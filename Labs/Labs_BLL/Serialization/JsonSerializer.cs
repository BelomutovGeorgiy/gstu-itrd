﻿using System.Collections.Generic;
using Labs_BLL.Abstractions.Serialization;
using Newtonsoft.Json;

namespace Labs_BLL.Serialization
{
    public class JsonSerializer<T> : IJsonSerializer<T> where T : class
    {
        public T Deserialize(string source) => JsonConvert.DeserializeObject<T>(source);
        public IEnumerable<T> DeserializeAll(string source) => JsonConvert.DeserializeObject<IEnumerable<T>>(source);
        public string Serialize(T source) => JsonConvert.SerializeObject(source);
        public string SerializeAll(IEnumerable<T> source) => JsonConvert.SerializeObject(source);
    }
}
