﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Labs_BLL.Abstractions.Serialization;

namespace Labs_BLL.Serialization
{
    public class XmlSerializer<T> : IXmlSerializer<T> where T : class
    {
        private readonly XmlSerializer _xmlSerializer;

        public XmlSerializer()
        {
            _xmlSerializer = new XmlSerializer(typeof(List<T>));
        }

        public T Deserialize(string source) =>
            (T)_xmlSerializer.Deserialize(new StringReader(source));

        public IEnumerable<T> DeserializeAll(string source) =>
            (IEnumerable<T>) _xmlSerializer.Deserialize(new StringReader(source));

        public string Serialize(T source)
        {
            var stringWriter = new StringWriter();
            _xmlSerializer.Serialize(stringWriter, source);
            return stringWriter.ToString();
        }

        public string SerializeAll(IEnumerable<T> source)
        {
            var stringWriter = new StringWriter();
            _xmlSerializer.Serialize(stringWriter, source);
            return stringWriter.ToString();
        }
    }
}
