﻿using Labs_DAL.Models;

namespace Labs_BLL.Abstractions
{
    public interface IConductorClient
    {
        void Create(Conductor item);

        void Update(int id, Conductor item);

        void Delete(int id);
    }
}
