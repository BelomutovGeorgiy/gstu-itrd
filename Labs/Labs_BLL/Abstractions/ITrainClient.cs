﻿using System.Collections.Generic;
using Labs_BLL.DTO;
using Labs_DAL.Models;

namespace Labs_BLL.Abstractions
{
    public interface ITrainClient
    {
        IEnumerable<TrainDTO> GetAll();

        TrainDTO GetById(int id);

        void Create(Train train);

        void Delete(int id);

        void Update(int id, Train train);
    }
}
