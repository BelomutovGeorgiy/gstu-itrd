﻿using System.Collections.Generic;

namespace Labs_BLL.Abstractions
{
    public interface IFileManager<T> where T : class
    {
        void SaveToFile(string fileName, IEnumerable<T> data);

        IEnumerable<T> ReadFromFile(string fileName);
    }
}
