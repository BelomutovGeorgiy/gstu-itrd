﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Labs_BLL.Abstractions.Kafka
{
    public interface IKafkaWrapper<T> where T: class
    {
        void Subscribe(string topic, Action<T> action, CancellationToken token);
        void SendMessage(string topic, T message);
    }
}
