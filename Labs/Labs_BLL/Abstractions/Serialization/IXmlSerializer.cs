﻿namespace Labs_BLL.Abstractions.Serialization
{
    public interface IXmlSerializer<T> : ISerializer<T> where T : class
    {
    }
}
