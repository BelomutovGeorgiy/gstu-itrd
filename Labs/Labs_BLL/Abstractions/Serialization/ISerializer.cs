﻿using System.Collections.Generic;

namespace Labs_BLL.Abstractions.Serialization
{
    public interface ISerializer<T> where T : class 
    {
        string Serialize(T source);
        string SerializeAll(IEnumerable<T> source);

        T Deserialize(string source);
        IEnumerable<T> DeserializeAll(string source);
    }
}
