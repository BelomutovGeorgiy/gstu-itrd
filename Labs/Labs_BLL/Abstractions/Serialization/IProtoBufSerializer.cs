﻿namespace Labs_BLL.Abstractions.Serialization
{
    public interface IProtoBufSerializer<T> : ISerializer<T> where T : class
    {
    }
}
