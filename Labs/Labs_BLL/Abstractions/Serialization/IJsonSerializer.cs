﻿namespace Labs_BLL.Abstractions.Serialization
{
    public interface IJsonSerializer<T> : ISerializer<T> where T : class
    {
    }
}
