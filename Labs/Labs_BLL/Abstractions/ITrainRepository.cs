﻿using System.Collections.Generic;
using Labs_DAL.Models;

namespace Labs_BLL.Abstractions
{
    interface ITrainRepository
    {
        IEnumerable<Train> GetAll();

        void Create(Train train);

        void Update(int id, Train train);

        void Delete(int id);

        Train Get(int id);

        void ReadCollection(string fileName);

        void WriteCollection(string fileName);
    }
}
