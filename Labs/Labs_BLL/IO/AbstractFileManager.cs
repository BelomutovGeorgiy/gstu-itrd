﻿using Labs_BLL.Abstractions;
using Labs_BLL.Abstractions.Serialization;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs_BLL.IO
{
    public abstract class AbstractFileWriter<T> : IFileManager<T> where T : class
    {
        protected IJsonSerializer<T> _jsonSerializer;
        protected IXmlSerializer<T> _xmlSerializer;
        protected IProtoBufSerializer<T> _protoBufSerializer;

        [Inject]
        public void Inject(IJsonSerializer<T> jsonSerializer,
            IXmlSerializer<T> xmlSerializer,
            IProtoBufSerializer<T> protoBufSerializer)
        {
            _jsonSerializer = jsonSerializer;
            _xmlSerializer = xmlSerializer;
            _protoBufSerializer = protoBufSerializer;
        }

        public abstract IEnumerable<T> ReadFromFile(string fileName);

        public abstract void SaveToFile(string fileName, IEnumerable<T> data);
    }
}
