﻿using System;
using System.Collections.Generic;
using Labs_DAL.Models;
using System.IO;

namespace Labs_BLL.IO
{
    public class TrainFileManager : AbstractFileWriter<Train>
    {
        public override IEnumerable<Train> ReadFromFile(string fileName)
        {
            var ext = Path.GetExtension(fileName);
            var fs = new FileStream(fileName, FileMode.Open);
            var sw = new StreamReader(fs);
            var data = sw.ReadToEnd();
            sw.Close();
            fs.Close();

            IEnumerable<Train> trains = new List<Train>();
            switch (ext)
            {
                case ".json":
                    trains = _jsonSerializer.DeserializeAll(data);
                    break;
                case ".xml":
                    trains = _xmlSerializer.DeserializeAll(data);
                    break;
                case ".protobuf":
                    trains = _protoBufSerializer.DeserializeAll(data);
                    break;
                default: break;
            }

            return trains;
        }

        public override void SaveToFile(string fileName, IEnumerable<Train> data)
        {
            var ext = Path.GetExtension(fileName);
            var str = string.Empty;
            switch (ext)
            {
                case ".json":
                    str = _jsonSerializer.SerializeAll(data);
                    break;
                case ".xml":
                    str = _xmlSerializer.SerializeAll(data);
                    break;
                case ".protobuf":
                    str = _protoBufSerializer.SerializeAll(data);
                    break;
                default: break;
            }
            var fs = new FileStream(fileName, FileMode.Create);
            var sw = new StreamWriter(fs);
            sw.Write(str);
            sw.Close();
            fs.Close();
        }
    }
}
