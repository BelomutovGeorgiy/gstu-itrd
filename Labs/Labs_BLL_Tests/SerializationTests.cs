﻿using System;
using System.Collections.Generic;
using System.Linq;
using Labs_BLL.Serialization;
using Labs_DAL.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Labs_BLL_Tests
{
    [TestClass]
    public class SerializationTests
    {
        private readonly IEnumerable<Train> _trains = new List<Train>
        {
            new Train
            {
                Id = 0,
                Number = 255854,
                ArriveTime = new DateTime(2012, 12, 20),
                Destination = "silent hill",
                TrainType = TrainType.Commodity,
                StartTime = new DateTime(2000, 12, 11),
                Origin = "home",
            },
            new Train
            {
                Id = 1,
                Number = 255681,
                ArriveTime = new DateTime(2012, 12, 20),
                Destination = "tuda",
                TrainType = TrainType.Fast,
                StartTime = new DateTime(2000, 12, 11),
                Origin = "suda",
            },
            new Train
            {
                Id = 2,
                Number = 259690,
                ArriveTime = new DateTime(2012, 12, 20),
                Destination = "moskow",
                TrainType = TrainType.Passenger,
                StartTime = new DateTime(2000, 12, 11),
                Origin = "kair",
            },
            new Train
            {
                Id = 3,
                Number = 152520,
                ArriveTime = new DateTime(2012, 12, 20),
                Destination = "hell",
                TrainType = TrainType.Passenger,
                StartTime = new DateTime(2000, 12, 11),
                Origin = "heaven",
            },
            new Train
            {
                Id = 4,
                Number = 668452,
                ArriveTime = new DateTime(2012, 12, 20),
                Destination = "earth",
                TrainType = TrainType.Express,
                StartTime = new DateTime(2000, 12, 11),
                Origin = "mars",
            }
        };

        [TestMethod]
        public void XmlSerialization()
        {
            // Arrange
            var xmlSerializer = new XmlSerializer<Train>();

            //Act
            var result = xmlSerializer.DeserializeAll(xmlSerializer.SerializeAll(_trains));

            //Assert
            CollectionAssert.AreEqual(_trains.ToList(), result.ToList());
        }

        [TestMethod]
        public void JsonSerialization()
        {
            // Arrange
            var jsonSerializer = new JsonSerializer<Train>();

            //Act
            var result = jsonSerializer.DeserializeAll(jsonSerializer.SerializeAll(_trains));

            //Assert
            CollectionAssert.AreEqual(_trains.ToList(), result.ToList());
        }

        [TestMethod]
        public void ProtoBufSerialization()
        {
            // Arrange
            var protobufSerializer = new ProtobufSerializer<Train>();

            //Act
            var result = protobufSerializer.DeserializeAll(protobufSerializer.SerializeAll(_trains));

            //Assert
            CollectionAssert.AreEqual(_trains.ToList(), result.ToList());
        }
    }
}
