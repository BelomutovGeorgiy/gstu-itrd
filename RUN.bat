echo off
set /a n=10
set /a n2=0
:start
cls
echo EQSS 1.0 by Belomutov George
echo Current Number Of Variables if %n%
echo 1 - solve by LDLt paralleling method
echo 2 - solve by LDLt linear method
echo 3 - Re-Generate matrix
echo 4 - Change variables number
echo 9 - Exit
echo
set /p var="You Choise: "
if %var% equ 1 goto first
if %var% equ 2 goto second
if %var% equ 3 goto thrid
if %var% equ 4 goto four
if %var% equ 9 goto nine
goto start
:first
cd labs\CP-LLt\bin\Debug
set /a n2 = %n%+1
mpiexec.exe -n %n2% CP-LLt.exe %n%
pause
cd ..\..\..\..
goto start
:second
cd labs\CP-Gauss\bin\Debug
CP-Gauss.exe %n%
pause
cd ..\..\..\..
goto start
:thrid
cd labs\CP-DataGen\bin\Debug
CP-DataGen.exe %n%
pause
cd ..\..\..\..
goto start
:four
set /p n="Select new value:"
goto start
:nine
Exit